/*
 * @Author: 糊涂的老知青
 * @Date: 2022-08-10
 * @Version: 1.0.0
 * @Description:
 */
package workflow

import (
	workflow_application "lzq-admin/internal/workflow/application"
	"lzq-admin/middleware"

	"github.com/gin-gonic/gin"
)

func WorkflowRouter(router *gin.RouterGroup) {
	router.Use(middleware.CheckJwtToken())
	{
		designerRouter := router.Group("/designer").Use()
		{
			designerRouter.GET("/list", workflow_application.IDesignerAppService.GetList)
			designerRouter.GET("/getTemplateById", workflow_application.IDesignerAppService.GetEntityTemplate)
		}

	}
}
