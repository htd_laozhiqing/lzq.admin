/*
 * @Author: 糊涂的老知青
 * @Date: 2022-09-07
 * @Version: 1.0.0
 * @Description:
 */
package workflow_domainservice

import (
	"lzq-admin/base/domainservice"
	workflow_model "lzq-admin/internal/workflow/model"
	"lzq-admin/pkg/orm"
	"lzq-admin/pkg/snowflake"

	"github.com/gin-gonic/gin"
)

type WfEntityDomainService struct {
	domainservice.BaseDomainService
}

func NewDSWfEntity(c *gin.Context) *WfEntityDomainService {
	return &WfEntityDomainService{
		domainservice.BaseDomainService{
			GinCtx: c,
		},
	}
}

func (ds *WfEntityDomainService) Insert(inputDto workflow_model.NodeDesignerDto) (workflow_model.NodeDesignerDto, error) {
	wfEntity := workflow_model.WfEntity{
		WfEntityBase: workflow_model.WfEntityBase{
			Name:        inputDto.WorkFlowDef.Name,
			Code:        inputDto.WorkFlowDef.Code,
			Status:      inputDto.WorkFlowDef.Status,
			PcUrl:       inputDto.WorkFlowDef.PcUrl,
			AppUrl:      inputDto.WorkFlowDef.AppUrl,
			RefType:     inputDto.WorkFlowDef.RefType,
			Version:     inputDto.WorkFlowDef.Version,
			Rank:        inputDto.WorkFlowDef.Rank,
			RefRecordId: inputDto.WorkFlowDef.RefRecordId,
		},
	}
	wfEntity.ID = snowflake.CreateSnowflakeId()
	lzqOrm := orm.NewLzqOrm(ds.GinCtx)
	dbSession, _ := lzqOrm.BeginTrans()
	if _, err := lzqOrm.ISessionWithTrans(dbSession).Insert(&wfEntity); err != nil {
		return inputDto, err
	}

	// wfEntityNode := workflow_model.WfEntityNode{
	// 	WfEntityNodeBase: workflow_model.WfEntityNodeBase{
	// 		NodeName: inputDto.NodeConfig.,
	// 	},
	// }
	inputDto.EntityId = wfEntity.ID
	return inputDto, nil
}
