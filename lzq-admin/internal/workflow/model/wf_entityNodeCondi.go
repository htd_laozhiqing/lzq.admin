/*
 * @Author: 糊涂的老知青
 * @Date: 2022-09-02
 * @Version: 1.0.0
 * @Description:
 */
package workflow_model

import "lzq-admin/base/model"

func (WfEntityNodeCondi *WfEntityNodeCondi) TableName() string {
	return TableWfEntityNodeCondi
}

type WfEntityNodeCondi struct {
	model.BaseModel       `xorm:"extends"`
	model.TenantBaseModel `xorm:"extends"`
	WfEntityNodeCondiBase `xorm:"extends"`
}

type WfEntityNodeCondiBase struct {
	TpltNodeId  string `json:"tpltNodeId" binding:"required" xorm:"varchar(36) notnull comment('模板节点Id')"`    //模板节点Id
	ConditionId string `json:"conditionId" xorm:"varchar(36) comment('条件ID（设计器生成的时间戳）')"`                     // 条件ID（设计器生成的时间戳）
	ShowName    string `json:"showName" binding:"required" xorm:"varchar(200) notnull comment('条件名称')"`       //条件名称
	ShowType    string `json:"showType" xorm:"varchar(200) comment('条件类型')"`                                  //条件类型
	ColumnId    string `json:"columnId" xorm:"varchar(36) comment('条件列ID')"`                                  //条件列ID
	ColumnCode  string `json:"columnCode" binding:"required" xorm:"varchar(200) notnull comment('条件编码')"`     //条件编码
	ColumnType  string `json:"columnType" binding:"required" xorm:"varchar(200) notnull comment('条件字段数据类型')"` //条件字段数据类型
	Type        string `json:"type" binding:"required" xorm:"varchar(200) notnull comment('条件接入类型')"`         //条件接入类型：fixedForm
	Zdy1        string `json:"zdy1" binding:"required" xorm:"varchar(200) notnull comment('条件满足值1')"`         //条件满足值1
	Zdy2        string `json:"zdy2" xorm:"varchar(200) comment('条件满足值2')"`                                    //条件满足值2
	OptType     string `json:"optType" binding:"required" xorm:"varchar(36) notnull comment('条件值对比运算符')"`     //条件值对比运算符
	OrIndex     int    `json:"orIndex" binding:"required" xorm:"default(0) comment('当前条件节点或条件的索引值0开始')"`      //当前条件节点或条件的索引值0开始
}

type WfEntityNodeCondiDto struct {
	WfEntityNodeCondiBase `xorm:"extends"`
}
