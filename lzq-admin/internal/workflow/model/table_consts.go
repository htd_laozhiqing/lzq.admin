/*
 * @Author: 糊涂的老知青
 * @Date: 2022-08-24
 * @Version: 1.0.0
 * @Description:
 */
package workflow_model

const (
	TableWfEntity          = "wf_entity"
	TableWfEntityNode      = "wf_entity_node"
	TableWfEntityNodeUser  = "wf_entity_node_user"
	TableWfEntityNodeCondi = "wf_entity_node_condi"
	TableWfBranchCondition = "wf_branch_condition"
)
