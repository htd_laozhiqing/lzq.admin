/*
 * @Author: 糊涂的老知青
 * @Date: 2022-08-26
 * @Version: 1.0.0
 * @Description:
 */
package workflow_model

import "lzq-admin/base/model"

func (WfEntityNode *WfEntityNode) TableName() string {
	return TableWfEntityNode
}

type WfEntityNode struct {
	model.BaseModel       `xorm:"extends"`
	model.TenantBaseModel `xorm:"extends"`
	WfEntityBase          `xorm:"extends"`
}

type WfEntityNodeBase struct {
	NodeName          string `json:"nodeName" binding:"required" xorm:"varchar(200) notnull comment('节点名称')"`        //节点名称
	EntityTemplateId  string `json:"entityTemplateId" binding:"required" xorm:"varchar(36) notnull comment('模板Id')"` //模板Id
	Type              string `json:"type" binding:"required" xorm:"varchar(200) notnull comment('节点类型')"`            //节点类型
	PriorityLevel     int    `json:"priorityLevel" xorm:"comment('条件优先级')"`                                          //条件优先级
	SelectRange       string `json:"selectRange" xorm:"varchar(200) comment('选择范围')"`                                //选择范围
	ExamineMode       string `json:"examineMode" xorm:"varchar(200) comment('单人或多人审批时采用的审批方式')"`                     //单人或多人审批时采用的审批方式
	NoHanderAction    string `json:"noHanderAction" xorm:"varchar(200) comment('审批人为空')"`                            //审批人为空时 1自动审批通过/不允许发起 2转交给审核管理员
	InitiatorOptional bool   `json:"initiatorOptional" xorm:"tinyint(1) comment('允许发起人自选审批人')"`                      //允许发起人自选审批人
	MaxOrIndex        int    `json:"maxOrIndex" xorm:"comment('或条件最大层级')"`                                           //或条件最大层级
}

type WfEntityNodeDto struct {
	WfEntityNodeBase `xorm:"extends"`
	ChildNode        WfEntityNodeDto        `json:"childNode"`      //子节点
	ConditionNodes   []WfEntityNodeDto      `json:"conditionNodes"` //分支条件节点
	ConditionList    []WfEntityNodeCondiDto `json:"conditionList"`  //条件集合
	NodeUserList     []WfEntityNodeUserDto  `json:"nodeUserList"`   //审批人集合
	Error            bool                   `json:"error"`          //节点是否异常
}
