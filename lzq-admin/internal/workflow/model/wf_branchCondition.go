/*
 * @Author: 糊涂的老知青
 * @Date: 2022-08-21
 * @Version: 1.0.0
 * @Description:
 */
package workflow_model

import "lzq-admin/base/model"

func (WfBranchCondition *WfBranchCondition) TableName() string {
	return TableWfBranchCondition
}

type WfBranchCondition struct {
	model.BaseModel       `xorm:"extends"`
	model.TenantBaseModel `xorm:"extends"`
	WfBranchConditionBase `xorm:"extends"`
}

type WfBranchConditionBase struct {
	ShowName          string `json:"showName" binding:"required" xorm:"varchar(200) notnull comment('分支条件名称')"`    //分支条件名称
	ShowType          string `json:"showType" binding:"required" xorm:"varchar(200) notnull comment('分支条件类型')"`    //分支条件类型 1：规定类型
	ColumnId          string `json:"columnId" xorm:"varchar(200) comment('分支条件字段ID')"`                             //分支条件字段ID
	ColumnCode        string `json:"columnCode" binding:"required" xorm:"varchar(200) notnull comment('分支条件编码')"`  //分支条件编码
	ColumnType        string `json:"columnType" binding:"required" xorm:"varchar(200) notnull comment('分支条件值类型')"` //分支条件值类型 string，datetime，decimal
	FixedDownBoxValue string `json:"fixedDownBoxValue"  xorm:"varchar(500) notnull comment('分支条件值范围')"`            //分支条件值范围
}
