/*
 * @Author: 糊涂的老知青
 * @Date: 2022-09-02
 * @Version: 1.0.0
 * @Description:
 */
package workflow_model

import "lzq-admin/base/model"

func (WfEntityNodeUser *WfEntityNodeUser) TableName() string {
	return TableWfEntityNodeUser
}

type WfEntityNodeUser struct {
	model.BaseModel       `xorm:"extends"`
	model.TenantBaseModel `xorm:"extends"`
	WfEntityNodeUserBase  `xorm:"extends"`
}

type WfEntityNodeUserBase struct {
	TpltNodeId string `json:"tpltNodeId" binding:"required" xorm:"varchar(36) notnull comment('模板节点Id')"` //模板节点Id
	Type       string `json:"type" binding:"required" xorm:"varchar(200) notnull comment('用户类型')"`        //用户类型，user，role,dept
	TargetId   string `json:"targetId" binding:"required" xorm:"varchar(36) notnull comment('审批人或角色ID')"` //审批人或角色ID
	Name       string `json:"name" binding:"required" xorm:"varchar(200) notnull comment('审批人或角色名称')"`    //审批人或角色名称
}

type WfEntityNodeUserDto struct {
	WfEntityNodeUserBase `xorm:"extends"`
}
