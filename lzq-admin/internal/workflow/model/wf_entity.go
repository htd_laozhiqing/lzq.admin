/*
 * @Author: 糊涂的老知青
 * @Date: 2022-08-21
 * @Version: 1.0.0
 * @Description:
 */
package workflow_model

import "lzq-admin/base/model"

func (WfEntity *WfEntity) TableName() string {
	return TableWfEntity
}

type WfEntity struct {
	model.BaseModel       `xorm:"extends"`
	model.TenantBaseModel `xorm:"extends"`
	WfEntityBase          `xorm:"extends"`
}

type WfEntityBase struct {
	Name        string `json:"name" binding:"required" xorm:"varchar(50) notnull comment('审批类型名称')"`        //审批类型名称
	Code        string `json:"code" binding:"required" xorm:"varchar(20) notnull comment('审批类型编码')"`        //审批类型编码
	Rank        int    `json:"rank" xorm:"default(1) notnull comment('排序')"`                                //排序
	Status      string `json:"status" xorm:"notnull comment('状态')"`                                         //状态
	PcUrl       string `json:"pcurl" binding:"required" xorm:"varchar(500) notnull comment('PC端审批地址')"`     //PC端审批地址
	AppUrl      string `json:"apprl" binding:"required" xorm:"varchar(500) notnull comment('移动端审批地址')"`     //移动端审批地址
	RefType     string `json:"refType" binding:"required" xorm:"varchar(200) notnull comment('模板类型')"`      //模板型
	RefRecordId string `json:"refRecordId" binding:"required" xorm:"varchar(36) notnull comment('模板类型ID')"` //模类型ID
	Version     string `json:"version" xorm:"varchar(200) default(0) notnull comment('版本号')"`               //版本号
}

type WfEntityInfoDto struct {
	ID string `json:"id" binding:"required"` //审批类型ID
	WfEntityBase
}

type NodeDesignerDto struct {
	EntityId         string                  `json:"entityId"`         //审批类型ID或表单ID
	WorkFlowDef      NodeDesignerWorkFlowDto `json:"workFlowDef"`      //审批类型或表单信息
	DirectorMaxLevel int                     `json:"directorMaxLevel"` //审批主管最大层级
	FlowPermission   string                  `json:"flowPermission"`   //
	NodeConfig       WfEntityNodeDto         `json:"nodeConfig"`       //节点信息
}

type NodeDesignerWorkFlowDto struct {
	WfEntityBase
}
