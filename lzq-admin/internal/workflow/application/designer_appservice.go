/*
 * @Author: 糊涂的老知青
 * @Date: 2022-08-10
 * @Version: 1.0.0
 * @Description:
 */

package workflow_application

import (
	"lzq-admin/base/application"

	"github.com/gin-gonic/gin"
)

type DesignerAppService struct {
	application.BaseAppService
}

var IDesignerAppService = DesignerAppService{}

// @Summary 审批类型列表
// @Tags Designer
// @Description
// @Security ApiKeyAuth
// @Accept mpfd
// @Produce  json
// @Param object query application.PageParamsDto true " "
// @Success 200 {array} workflow_model.NodeDesignerDto
// @Failure 500 {object} application.ResponseDto
// @Router /api/workflow/desiger/list [GET]
func (app *DesignerAppService) GetList(c *gin.Context) {
	app.ResponseSuccess(c, "")
	return
}

// @Summary 查询审批类型流程模板
// @Tags Designer
// @Description
// @Security ApiKeyAuth
// @Accept mpfd
// @Produce  json
// @Param tpltId query string true "流程模板ID"
// @Success 200 {object} workflow_model.NodeDesignerDto " "
// @Failure 500 {object} application.ResponseDto
// @Router /api/workflow/desiger/getTemplateById [GET]
func (app *DesignerAppService) GetEntityTemplate(c *gin.Context) {
	tpltId := c.Query("tpltId")
	app.ResponseSuccess(c, tpltId)
	return
}
