/*
 * @Author: 糊涂的老知青
 * @Date: 2021-10-30
 * @Version: 1.0.0
 * @Description:
 */
package main

import (
	"fmt"
	"lzq-admin/config"
	"lzq-admin/config/appsettings"
	_ "lzq-admin/pkg/cache"
	"lzq-admin/pkg/hsflogger"
	_ "lzq-admin/pkg/hsflogger"
	"lzq-admin/pkg/orm"
	"lzq-admin/pkg/snowflake"
	"lzq-admin/router"
	"net/http"

	"github.com/gin-gonic/gin"
)

// @title lzq-admin Project API
// @version 1.0
// @description  Golang api of demo
// @termsOfService http://github.com

// @contact.name API Support
// @contact.url http://www.lzqit.cn
// @contact.email lzqcode@163.com

// @host localhost:30001
// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization
// 程序入口
func main() {
	hsflogger.Init()
	// 配置初始化
	gin.SetMode(config.Config.ServerConfig.RunMode)
	// 数据库连接初始化
	orm.DatabaseInit()
	// 业务配置初始化
	appsettings.Init()
	// 初始化雪花算法参数
	snowflake.InitSnowflake(0)
	// 初始化消息总线
	//eventbus.EventBusInit()
	// 装载路由
	r := router.Init()
	server := &http.Server{
		Addr:    fmt.Sprintf(":%v", config.Config.ServerConfig.HttpPort),
		Handler: r,
	}

	_ = server.ListenAndServe()

}
