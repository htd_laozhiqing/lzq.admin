/*
 * @Author: 糊涂的老知青
 * @Date: 2022-07-30
 * @Version: 1.0.0
 * @Description:
 */
package cache

import (
	"fmt"

	"github.com/gin-gonic/gin"
)

/**
 * @Author  糊涂的老知青
 * @Date    2022/2/26
 * @Version 1.0.0
 */

var LzqCacheKeyHelper struct {
}

func NewLzqCacheKey(c *gin.Context) *cacheKeyHelper {
	return &cacheKeyHelper{
		ginCtx: c,
	}
}

type cacheKeyHelper struct {
	ginCtx *gin.Context
}

func (c *cacheKeyHelper) GetUserGrantedMenusCacheKey(userId string) string {
	return fmt.Sprintf("%v:UserGrantedMenus:%v", LzqCacheHelper.GetCacheVersion(c.ginCtx, LzqCacheTypeFunctionPrivilege), userId)
}

func (c *cacheKeyHelper) GetRoleGrantedPermissionsCacheKey(roleId string) string {
	return fmt.Sprintf("%v:RoleGrantedPermissions:%v", LzqCacheHelper.GetCacheVersion(c.ginCtx, LzqCacheTypeRolePrivilege), roleId)
}

func (c *cacheKeyHelper) GetGrantedDataPrivilegeByUserCacheKey(userId string) string {
	return fmt.Sprintf("%v:GrantedDataPrivilegeByUser:%v", LzqCacheHelper.GetCacheVersion(c.ginCtx, LzqCacheTypeDataPrivilege), userId)
}

func (c *cacheKeyHelper) GetUserGrantedPolicyCacheKey(userId string) string {
	return fmt.Sprintf("%v:UserGrantedPolicy:%v", LzqCacheHelper.GetCacheVersion(c.ginCtx, LzqCacheTypeFunctionPrivilege), userId)
}

func (c *cacheKeyHelper) GetUserGrantedPermissionsCacheKey(userId string) string {
	return fmt.Sprintf("%v:%v:UserGrantedPermissions:%v", LzqCacheHelper.GetCacheVersion(c.ginCtx, LzqCacheTypeFunctionPrivilege), LzqCacheHelper.GetCacheVersion(c.ginCtx, LzqCacheTypeDataPrivilege), userId)
}

func (c *cacheKeyHelper) GetLzqCacheTypeSystemDict(dictCode string) string {
	return fmt.Sprintf("SystemDict:%v:%v", LzqCacheHelper.GetCacheVersion(c.ginCtx, LzqCacheTypeSystemDict), dictCode)
}
