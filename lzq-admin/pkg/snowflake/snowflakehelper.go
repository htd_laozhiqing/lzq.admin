/*
 * @Author: 糊涂的老知青
 * @Date: 2022-08-14
 * @Version: 1.0.0
 * @Description:
 */
package snowflake

import (
	"time"
)

var node *Node

func InitSnowflake(machineID int64) (err error) {
	var st time.Time
	st, err = time.Parse("2006-01-02", "2022-01-01")
	if err != nil {
		return
	}
	Epoch = st.UnixNano() / 1000000
	node, err = NewNode(machineID)
	return
}
func CreateSnowflakeId() string {
	return node.Generate().String()
}
