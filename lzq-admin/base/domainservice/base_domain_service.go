/*
 * @Author: 糊涂的老知青
 * @Date: 2022-07-30
 * @Version: 1.0.0
 * @Description:
 */
package domainservice

/**
 * @Author  糊涂的老知青
 * @Date    2022/7/25
 * @Version 1.0.0
 */
import "github.com/gin-gonic/gin"

type BaseDomainService struct {
	GinCtx *gin.Context
}
