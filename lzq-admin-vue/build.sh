npm install --registry=https://registry.npm.taobao.org
chmod -R 7777 node_modules
npm run build:prod
cp Dockerfile dist/
cp nginx.conf dist/
cd dist
docker build -t lzq-admin-f:0.0.1 .
